# Task Manager

I'm a person who passionate about my own productivity. \
I want to manage my tasks and projects more effectively. \
I need a simple tool that supports me in controlling my task-flow.

## Functional requirements

 - [x] I want to be able to create/update/delete projects
 - [x] I want to be able to add tasks to my project
 - [x] I want to be able to update/delete tasks
 - [ ] I want to be able to prioritize tasks into a project
 - [ ] I want to be able to choose deadline for my task
 - [x] I want to be able to mark a task as 'done'

## Technical requirements

1. It should be a WEB application
2. For the client side must be used:
    - HTML, CSS (any libs as Twitter Bootstrap, Blueprint, ...)
    - JavaScript (any libs a s jQuery, Prototype, ...)
3. For a server side any language as Ruby, PHP, Python, JavaScript, C#, Java, ...
4. It should have a client side and server side validation
5. It should look like on picture

<img src="test_task_view.png" width="70%" style="margin-left:15%">

## Additional requirements
 - [x] It should work like one page WEB application 
 - [ ] It should use AJAX technology, load and submit data without reloading a page.
 - [ ] It should have user authentication solutions and a user should only have access to their own projects and tasks.
 - [ ] It should have automated tests for the all functionality.


# SQL task

## Given tables

1. tasks (id, name, status, project_id)
2. projects (id, name)

UPD: I'm not sure about `status` field. \
It definetly should be BOOLEAN, because we just need to know if the task done or not. \
But if you really need to sort all statuses alphabetically, as in first task, then it supposed to be varchar.

UPD2: Oh, looks like I got an idea about `status` field (while solving last sql task). \
If it's necessary to define multiple values to this comumn, such as 'opened', 'validated', 'completed' etc., \
then the best way is to made `status` field an ENUM type. \
But there are nothing about it in task requirements... \
So confusing.

~~~sql
    CREATE DATABASE IF NOT EXISTS sql_task;
    USE sql_task;

    CREATE TABLE projects (
        id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(128) NOT NULL
    );

    CREATE TABLE tasks (
        id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(128) NOT NULL,
        status VARCHAR(128) NOT NULL,
        project_id INTEGER NOT NULL,
        FOREIGN KEY (project_id) REFERENCES projects(id) ON UPDATE CASCADE ON DELETE CASCADE
    );
~~~

## Technical requirements

- [x] get all statuses, not repeating, alphabetically ordered
~~~sql
    select distinct (status) from tasks order by status asc;
~~~

- [x] get the count of all tasks in each project, order by tasks count descending
~~~sql
    select project_id, count(name) from tasks 
    group by project_id order by count(name) desc;
~~~

- [x] get the count of all tasks in each projects, order by projects names
~~~sql
    select p.name, count(t.name) from projects p, tasks t 
    where p.id = t.project_id 
    group by p.id 
    order by p.name asc;
~~~

- [x] get the tasks for all projects having the name beginning with "N" letter
~~~sql
    select * from tasks where name like 'N%';
~~~

- [x] get the list of all projects containing the 'a' letter in the middle of the name, and show the tasks count near each project. Mention that there can exist projects without tasks and tasks with project_id=NULL.

UPD3: As I set up foreign key to cascade deletion, if project will be deleted, it's tasks also will be deleted as well. \
And in real-world case, task-without-project entities creation can be (easily?) avoided. \
So the "tasks with project_id=NULL" case seems like irrelevant. \
Thas's why my solution doesn't include that case.
But, of course, I may miss something upon that.
~~~sql
    select p.name, count(t.name) from projects p left join tasks t 
    on p.id = t.project_id
    where p.name like '%a%' 
    group by p.id 
    order by p.name asc;  
~~~

- [x] get the list of tasks with duplicate names. Order alphabetically
~~~sql
    select * from tasks t 
        inner join (select name from tasks 
                    group by name 
                    having count(name) > 1) dup 
        on t.name = dup.name
        order by t.name asc;
~~~

- [x] get list of tasks having several exact matches of both name and status, from the project 'Garage'. Order by matches count
~~~sql

    #FIRST VARIANT

    select t.name, t.status, t.name_count 
    from 
        (select * from projects where name = "Garage") p
    left join 
        (select name, status, project_id, count(name) as name_count
         from tasks
         group by name, status, project_id
         having count(name) > 1) t
    on p.id = t.project_id
    order by t.name_count desc;

    #SECOND VARIANT

    select p.name, t.name, t.status, count(t.name) 
    from projects p 
        left join tasks t on p.id=t.project_id  
        inner join (
            select name, status from tasks t1
            group by t1.name, t1.status
            having count(t1.name) > 1
        ) dup 
    where t.name = dup.name
        and t.status = dup.status
        and p.name = "Garage"
    group by t.name, t.status
    order by count(t.name);
~~~

- [x] get the list of project names having more than 10 tasks in status 'completed' Order by project_id.
~~~sql
    select t.project_id, p.name, t.status, count(t.status) 
    from projects p
        left join tasks t 
            on p.id = t.project_id
    where status = 'completed'
    group by p.name, t.status, t.project_id
    having count(t.status) > 10
    order by t.project_id;
~~~
