const env = process.env.NODE_ENV;

const dev = {
  backend:{
    host: 'localhost',
    port: 9090
  }
}

const prod = {
  backend:{
    host: 'lsamara.com',
    port: 9090
  }
}

const config = {
  dev, prod
}

module.exports = config[env];
