import { get, writable } from 'svelte/store';
const {backend: {host, port} } = require('./../../config.js');


const init_value = [
  {id: 1, name: "Your first project", taskList: [

  ] }
];

// const init_value = [
// 	{ id: 1, done: false, description: 'write some docs' },
// 	{ id: 2, done: false, description: 'start writing JSConf talk' },
// 	{ id: 3, done: true, description: 'buy some milk' },
// 	{ id: 4, done: false, description: 'mow the lawn' },
// 	{ id: 5, done: false, description: 'feed the turtle' },
// 	{ id: 6, done: false, description: 'fix some bugs' },
// ];

// const init_value = [
//   { id:1, name:"Complete the test task for Ruby Garage", todoList:[
//     { id: 1, done: false, description: 'Open this mock-up in Adobe Fireworks' },
//     { id: 2, done: false, description: 'Alternatively check file' },
//     { id: 3, done: false, description: 'Write HTML & CSS' },
//     { id: 4, done: false, description: 'Add Javascript to implement adding / editing / removing for todo items and lists taking into account as more use cases as possible ' },
//   ]},
//   { id:2, name:"For Home", todoList:[
//     { id: 1, done: false, description: 'Buy a milk' },
//     { id: 2, done: false, description: 'Call Mom' },
//     { id: 3, done: false, description: 'Clean the room' },
//     { id: 4, done: false, description: 'Repair the DVD Player' },
//   ]}
// ];


function createProjectStore(){
  const store = writable([])
  const { subscribe, set, update } = store;

  let new_proj_id = get(store).length + 1;

  return {
    subscribe,
    set,
    retrieveData: async () => {
      console.log("Retrieving data...");
      // set(init_value);
      const response = await fetch(`http://${host}:${port}/projects`);
      let projects = await response.json();

      // projects.forEach(async (proj) => {
      //   const tr = await fetch(`http://localhost:9090/projects/${proj.id}/tasks`);
      //   let tasks = await tr.json();
      //   proj.taskList = tasks.length !== 0 ? tasks : [];
      //   let item = {};
      //   item.id = "DAAD";
      //   item.name = "ASDASD";
      //   item.taskList = "aqweasd";
      //   return item;
      // });
      // projects = projects;

      console.log(JSON.stringify(projects));
      console.log(projects);
      console.log("Done!");
      set(projects);
    },

    sendData: () => {},

    byPID: (project_id) => {
      return get(store).find(p => p.id === project_id);
    },
  };
}

export const projects = createProjectStore();
