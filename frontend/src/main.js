//just an examples from Svelte official site
import App from './App.svelte';
import Mytable from './Mytable.svelte'

//working all-in-one TODO component
// import Todo from './components/MyTodo.svelte'

//divided into components version of TODO
import Todo from './components/Board.svelte'

//for fetch testing purpose
// import Todo from './components/FetchTest.svelte'


const app = new Todo({
	target: document.body,
	props: {
		name: 'world'
	}
});
export default app;
