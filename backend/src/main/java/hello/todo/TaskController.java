package hello.todo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
public class TaskController {
    @Autowired
    public TaskRepository taskRepository;

    @Autowired
    public ProjectRepository projectRepository;

//    @GetMapping("/projects/{projectId}/tasks")
//    public Page<Task> getAllTasksByProjectId(
//            @PathVariable(value = "projectId") Long projectId,
//            Pageable pageable
//    ){
//        return taskRepository.findProjectById(projectId, pageable);
//    }

    @GetMapping("/projects/{projectId}/tasks")
    public List<Task> getAllTasksByProjectId(
            @PathVariable(value = "projectId") Long projectId
    ){
        return taskRepository.findByProjectId(projectId);
    }

    @PostMapping("/projects/{projectId}/tasks")
    public Task createTask(
            @PathVariable(value = "projectId") Long projectId,
            @Valid @RequestBody Task task
    ){
        return projectRepository.findById(projectId).map(project -> {
            task.setProject(project);
            return taskRepository.save(task);
        }).orElseThrow(() -> new ResourceNotFoundException("ProjectId " + projectId + " not found"));
    }

    @PutMapping("/projects/{projectId}/tasks/{taskId}")
    public Task updateTask(
            @PathVariable (value = "projectId") Long projectId,
            @PathVariable (value = "taskId") Long taskId,
            @Valid @RequestBody Task taskRequest

    ){
        if (!projectRepository.existsById(projectId)) {
            throw new ResourceNotFoundException("ProjectId " + projectId + " not found");
        }

        return taskRepository.findById(taskId).map(task -> {
            task.setName(taskRequest.getName());
            task.setStatus(taskRequest.getStatus());
            return taskRepository.save(task);
        }).orElseThrow(() -> new ResourceNotFoundException("TaskId " + taskId+ " not found"));
    }

    @DeleteMapping("/projects/{projectId}/tasks/{taskId}")
    public ResponseEntity<?> deleteTask(
            @PathVariable (value = "projectId") Long projectId,
            @PathVariable (value = "taskId") Long taskId
    ){
        return taskRepository.findByIdAndProjectId(taskId, projectId).map(task -> {
            taskRepository.delete(task);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("TaskId " + taskId+ " not found"));
    }
}
